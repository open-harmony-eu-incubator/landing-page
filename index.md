**INTRODUCTION**

OpenHarmony is an open-source project launched by Huawei with the aim of embracing the promise of a versatile and all-scenario intelligent ecosystem. OpenHarmony repository can be found [here](https://openharmony.gitee.com "here").

Huawei's Open Source Technology Center in Europe main goal is that to adapt and extend OpenHarmony to the needs of the European market, by the mean of cooperating with European partners to co-developing a shared set of goals and objectives, a roadmap for key activities and functionalities and ultimately work jointly to implement such functionalities that power real devices.

This repository, OpenHarmony EU Incubator, is meant to serve as the common playground where initial co-operations can happen. Ultimately, this repository will find its proper house and be hosted by a European open source Foundation.

The innovative idea in OpenHarmony is about transcending the traditional relation between a device and its operating system, and instead creating a new intelligent and collaborative scenario where devices collaborate by sharing their resources, while OpenHarmony acts as the technology that enables such smooth, efficient and transparent collaboration.

OpenHarmony utilizes a component-based design to tailor its features to better suit specific devices, based on each device's capabilities and service characteristics.

<img src="https://gitee.com/open-harmony-eu-incubator/landing-page/raw/master/gitee_ohos.png"  width="700" height="500">

**FEATURES ROADMAP**

This section captures the functionalities that are currently under consideration and development.

| **Functionality** | **Area** | **State** | **Branch** | **Priority** |
| --- | --- | --- | --- | --- |
| Introduction of bitbake as underlying build system | Build tools | In design | development | High |
| Yocto Project 4.19 Linux kernel | Kernel | In design | development | High |
| Yocto Project / OE Core GNU toolchain | Toolchain | In design | development | High |
| Adding sphynx for documentation generation | Documentation | In design | development | High |
| Adding SPDX and licenses manifest auto generation | FOSS compliance | In design | development | High |
| LiteOS QEMU Cortex-M4 BSP | Board Support Package | In development | development | High |
| LiteOS QEMU Cortex-A7 BSP | Board Support Package | In development | development | High |
| LiteOS rootfs templates definition | Userspace | In design | development | High |
| LiteOS 96boards Ivy BSP - Basic | Board Support Package | In development | development | Normal |
| LiteOS 96boards Avenger96 BSP - Basic | Board Support Package | In development | development | Normal |
| LiteOS ST Nucleo WB55RG BSP - Basic | Board Support Package | Not started | development | Normal |
| LiteOS 96boards Nitrogen BSP - Basic | Board Support Package | Not started | development | Normal |
| LiteOS 96boards Carbon BSP - Basic | Board Support Package | Not started | development | Normal |
| LiteOS Seco SBC-C61 BSP – Basic | Board Support Package | Not started | development | Normal |
| Linux Seco SBC-C61 BSP - Basic | Board Support Package | Not started | development | Normal |
| Linux Seco SBC-B68-eNUC BSP – Basic | Board Support Packages | Not started | development | Normal |
| Linux Seco SYS-C23-IGW BSP - Basic | Board Support Packages | Not started | development | Normal |
| Linux QEMU Cortex-A9 BSP - Basic | Board Support Packages | Not started | development | Normal |
| Linux QEMU RISC-V BSP - Basic | Board Support Packages | Not started | development | Normal |

**GETTING STARTED AND DOCUMENTATION**
Would you like to start? click [here](https://www.youtube.com/watch?v=iyz8i-zjCfE&feature=youtu.be)  and see the demo on how you can build the code and flash the image on the development board
 
For more information, please refer to [here](https://gitee.com/open-harmony-eu-incubator/docs/tree/master/docs-en)

**CONTRIBUTION**

If you are interested in Open Harmony and want to join our OpenHarmony EU Incubator please contact us at incubateohos@huawei.com

**TALK WITH US**

You can find us on IRC, [webchat.freenode.net](https://webchat.freenode.net/?channel=#ohos-eu-incubator), channel **#ohos-eu-incubator**.

